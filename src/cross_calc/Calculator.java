/** Copyright 2011 0x783czar (AKA Diego Carrion)
 * 	This Program is Distributed under the terms of the GPL
 * 
 * 	This file is part of CrossCalculator.
 * 
 * 	CrossCalculator is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  CrossCalculator is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**	Calculator Class
 * 
 * 	Designed to provide back-end functionality to a calculator program.
 * 		uses externally accessible data-fields to provide data to calculate
 * 
 * 	Version 0.0.1
 * 
 *
 *	Currently Provides the following Functionality:
 *		-addition
 *		-subtraction
 *		-multiplication
 *		-division
 *		-squaring
 *		-square-rooting
 */

package cross_calc;


public class Calculator {

	protected double element1 = 0d;
	protected double element2 = 0d;
	protected double ans;
	protected double memory = 0d;
	
	
	public double getElement1() {
		return element1;
	}



	public void setElement1(double element1) {
		this.element1 = element1;
	}



	public double getElement2() {
		return element2;
	}



	public void setElement2(double element2) {
		this.element2 = element2;
	}



	public double getAns() {
		return ans;
	}



	public void setAns(double ans) {
		this.ans = ans;
	}



	public double getMemory() {
		return memory;
	}



	public void setMemory(double memory) {
		this.memory = memory;
	}



	protected double add()	{
		double result = element1 + element2;
		ans = result;
		return result;
	}
	
	protected double subtract()	{
		double result = element1 - element2;
		ans = result;
		return result;
	}
	
	protected double multiply()	{
		double result = element1 * element2;
		ans = result;
		return result;
	}
	
	protected double divide()	{
		double result = element1 / element2;
		ans = result;
		return result;
	}
	
	protected double square()	{
		double result = element1 * element1;
		ans = result;
		return result;
	}
	
	protected double squareRoot()	{
		double result = Math.sqrt(element1);
		ans = result;
		return result;
	}
}

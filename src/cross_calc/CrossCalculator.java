/** Copyright 2011 0x783czar (AKA Diego Carrion)
 * 	This Program is Distributed under the terms of the GPL
 * 
 * 	This file is part of CrossCalculator.
 * 
 * 	CrossCalculator is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  CrossCalculator is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**	CrossCalculator: 
 * 
 * 	A Simple, Cross-Platform, Calculator.
 * 		Designed to mimic the appearance and operation of a basic calculator
 * 		Relies on Calculator.java to provide back-end functionality
 * 
 * 	Version: 0.0.1-alpha
 * 
 * 
 * 	File Structure
 * 
 * 	class A16_4
 * 		create GUI
 * 		class ButtonListener
 * 			assign functions to buttons
 * 	class ScreenPanel
 * 		draw a screen with graphics
 * 		provide data fields and functions to modify screen
 */

/** Version notes:
 * 
 * 	Known Issues:
 * 
 * 
 */

package cross_calc;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


public class CrossCalculator {
	
	/** Data Fields */
	
	ScreenPanel screen = new ScreenPanel();			// create a ScreenPanel graphics object to control the output of the screen
	
	// GUI Buttons
	JButton jbt0 = new JButton("0");				// input a 0
	JButton jbt1 = new JButton("1");				// input a 1
	JButton jbt2 = new JButton("2");				// input a 2
	JButton jbt3 = new JButton("3");				// input a 3
	JButton jbt4 = new JButton("4");				// input a 4
	JButton jbt5 = new JButton("5");				// input a 5
	JButton jbt6 = new JButton("6");				// input a 6
	JButton jbt7 = new JButton("7");				// input a 7
	JButton jbt8 = new JButton("8");				// input an 8
	JButton jbt9 = new JButton("9");				// input a 9
	JButton jbtPoint = new JButton(".");			// input a decimal period
	JButton jbtPi = new JButton("\u03c0");			// input Pi
	JButton jbtAdd = new JButton("+");				// input a plus sign
	JButton jbtSubtract = new JButton("\u2014");	// input a minus sign.  IMPORTANT: this is a dash not a hyphen.  This is used to distinguish it from negative notation
	JButton jbtDivide = new JButton("\u00f7");		// input a divided-by sign
	JButton jbtMultiply = new JButton("\u00d7");	// input a times sign (note this is not an x but a times sign, but as the * is used in the programs operation, this doesn't matter)
	JButton jbtSquare = new JButton("x^2");			// square the currently displayed value
	JButton jbtSquareRoot = new JButton("\u221a");	// get the square root of the currently displayed value
	JButton jbtPosNeg = new JButton("\u00b1");		// flip the current numbers positivity
	JButton jbtClear = new JButton("C");			// clear the display
	JButton jbtBackspace = new JButton("Bksp");		// remove the last inputed character
	JButton jbtMemory = new JButton("M");			// load the last answer into memory
	JButton jbtMemoryRecall = new JButton("MR");	// recall the value stored in memory
	JButton jbtEquals = new JButton("=");			// perform the calculation shown and return the product
	
	
	/** No-Arg Constructor */	//Note: as it is private, only this class can access it.  Which is done from main.
	private CrossCalculator()	{
	
		/** Assign ActionListener to the GUI buttons */
		
		ButtonListener listener = new ButtonListener();
		
		jbt0.addActionListener(listener);
		jbt1.addActionListener(listener);
		jbt2.addActionListener(listener);
		jbt3.addActionListener(listener);
		jbt4.addActionListener(listener);
		jbt5.addActionListener(listener);
		jbt6.addActionListener(listener);
		jbt7.addActionListener(listener);
		jbt8.addActionListener(listener);
		jbt9.addActionListener(listener);
		jbtPoint.addActionListener(listener);
		jbtPi.addActionListener(listener);
		jbtAdd.addActionListener(listener);
		jbtSubtract.addActionListener(listener);
		jbtDivide.addActionListener(listener);
		jbtMultiply.addActionListener(listener);
		jbtSquare.addActionListener(listener);
		jbtSquareRoot.addActionListener(listener);
		jbtPosNeg.addActionListener(listener);
		jbtClear.addActionListener(listener);
		jbtBackspace.addActionListener(listener);
		jbtMemory.addActionListener(listener);
		jbtMemoryRecall.addActionListener(listener);
		jbtEquals.addActionListener(listener);
		

		/** Create Control Panel */		// Note: The control panel is split into two panels, this is done to balance the spacing issues inherent in GridLayout Managers
										//			thus the screen panel will not take up half of the window.
		
		JPanel controlPanel1 = new JPanel();
		controlPanel1.setLayout(new GridLayout(3, 4, 5, 5));
		
		controlPanel1.add(jbtClear);
		controlPanel1.add(jbtBackspace);
		controlPanel1.add(jbtMemory);
		controlPanel1.add(jbtMemoryRecall);
		controlPanel1.add(jbtPi);
		controlPanel1.add(jbtSquare);
		controlPanel1.add(jbtSquareRoot);
		controlPanel1.add(jbtMultiply);
		controlPanel1.add(jbt7);
		controlPanel1.add(jbt8);
		controlPanel1.add(jbt9);
		controlPanel1.add(jbtDivide);
		
		JPanel controlPanel2 = new JPanel();
		controlPanel2.setLayout(new GridLayout(3, 4, 5, 5));
		
		controlPanel2.add(jbt4);
		controlPanel2.add(jbt5);
		controlPanel2.add(jbt6);
		controlPanel2.add(jbtAdd);
		controlPanel2.add(jbt1);
		controlPanel2.add(jbt2);
		controlPanel2.add(jbt3);
		controlPanel2.add(jbtSubtract);
		controlPanel2.add(jbtPosNeg);
		controlPanel2.add(jbt0);
		controlPanel2.add(jbtPoint);
		controlPanel2.add(jbtEquals);

		
		/** Create Frame */
		
		JFrame frame = new JFrame("Calculator");
		frame.setLayout(new GridLayout(3, 1, 5, 5));	// added the last two 5s to make the spacing between panels appear even.
		
		frame.add(screen);				// loads the graphic component 'screen' into the first spot of the layout manager
		frame.add(controlPanel1);
		frame.add(controlPanel2);
		
		frame.setSize(310, 370);								// calculator size
		frame.setResizable(false);								// cannot be resized
		frame.setLocationRelativeTo(null);						// places the window in the center of the screen by default
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	
	/** Main Method */		// Program will call this first and it will call the constructor to initialize program operations
	public static void main(String[] args) {
		new CrossCalculator();
	}
	
	
	/** ButtonListener Class (ActionListener) */		// Note: this class is contained within the main class and can access all data fields of the containing class
	private class ButtonListener implements ActionListener	{
		
		protected Calculator c = new Calculator();		// create an object from the back-end class.  See Calculator.java
		protected char currentOperator;					// this data field will store the last mathematical operator used to control what sort of function is being preformed
		
		/** actionPreformed Method */					// Note: Overridden from ActionListener class.  Gives functionality to GUI components.
		@Override
		public void actionPerformed(ActionEvent e) {
			
			// 0 Button
			if (e.getSource() == jbt0 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("0");
			}
			// 1 Button
			if (e.getSource() == jbt1 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("1");
			}
			// 2 Button
			if (e.getSource() == jbt2 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("2");
			}
			// 3 Button
			if (e.getSource() == jbt3 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("3");
			}
			// 4 Button
			if (e.getSource() == jbt4 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("4");
			}
			// 5 Button
			if (e.getSource() == jbt5 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("5");
			}
			// 6 Button
			if (e.getSource() == jbt6 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("6");
			}
			// 7 Button
			if (e.getSource() == jbt7 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("7");
			}
			// 8 Button
			if (e.getSource() == jbt8 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("8");
			}
			// 9 Button
			if (e.getSource() == jbt9 && screen.appendable == true)	{		// check if the ScreenPanel is appendable
				screen.appendString("9");
			}
			// Decimal-Point Button
			if (e.getSource() == jbtPoint && screen.appendable == true && screen.postDecimal == false)	{	// check if the ScreenPanel is appendable & if a decimal has not already been placed
				if (screen.s == "0")	{
					screen.appendString("0.");
					screen.postDecimal = true;
				}
				else	{
					screen.appendString(".");
					screen.postDecimal = true;
				}
			}
			// Pi Button
			if (e.getSource() == jbtPi && screen.appendable == true)	{					// check if the ScreenPanel is appendable
				screen.appendString("3.14159265");
				screen.postDecimal = true;
			}
			// Add Button
			if (e.getSource() == jbtAdd && screen.operatorPresent == false)	{				// check to be sure no other operator is already present
				screen.appendString("+");
				currentOperator = '+';
				screen.postDecimal = false;
				screen.appendable = true;
				screen.operatorPresent = true;
				screen.memoryRecalled = false;
			}
			// Subtract Button
			if (e.getSource() == jbtSubtract && screen.operatorPresent == false)	{		// check to be sure no other operator is already present
				screen.appendString("\u2014");
				currentOperator = '\u2014';
				screen.postDecimal = false;
				screen.appendable = true;
				screen.operatorPresent = true;
				screen.memoryRecalled = false;
			}
			// Divide Button
			if (e.getSource() == jbtDivide && screen.operatorPresent == false)	{			// check to be sure no other operator is already present
				screen.appendString("/");
				currentOperator = '/';
				screen.postDecimal = false;
				screen.appendable = true;
				screen.operatorPresent = true;
				screen.memoryRecalled = false;
			}
			// Multiply Button
			if (e.getSource() == jbtMultiply && screen.operatorPresent == false)	{		// check to be sure no other operator is already present
				screen.appendString("*");
				currentOperator = '*';
				screen.postDecimal = false;
				screen.appendable = true;
				screen.operatorPresent = true;
				screen.memoryRecalled = false;
			}
			// Square Button
			if (e.getSource() == jbtSquare)	{
				c.element1 = Double.parseDouble(screen.s);
				screen.setString(Double.toString(c.square()));
				screen.appendable = false;
				screen.postDecimal = false;
				screen.operatorPresent = false;
			}
			// Square-Root Button
			if (e.getSource() == jbtSquareRoot)	{
				c.element1 = Double.parseDouble(screen.s);
				screen.setString(Double.toString(c.squareRoot()));
				screen.appendable = false;
				screen.postDecimal = false;
				screen.operatorPresent = false;
				screen.memoryRecalled = false;
			}
			// Positive-Negative Button
			if (e.getSource() == jbtPosNeg)	{
				screen.negate(currentOperator);
			}
			// Clear Button
			if (e.getSource() == jbtClear)	{
				screen.setString("0");
				c.element1 = 0;
				c.element2 = 0;
				screen.appendable = true;
				screen.postDecimal = false;
				screen.operatorPresent = false;
				screen.memoryRecalled = false;
			}
			// Backspace Button
			if (e.getSource() == jbtBackspace && screen.appendable == true)	{				// check if ScreenPanel is appendable
				// check if the character about to be erased is a decimal-point
				if (screen.s.charAt(screen.s.length() - 1) == '.')	{
					screen.postDecimal = false;
				}
				// check if the character about to be erased is an operator
				if (screen.s.charAt(screen.s.length() - 1) == '+' || screen.s.charAt(screen.s.length() - 1) ==  '\u2014' || screen.s.charAt(screen.s.length() - 1) ==  '*' || screen.s.charAt(screen.s.length() - 1) ==  '/')	{
					screen.operatorPresent = false;
				}
				screen.backspaceString();
				
			}
			// Memory Button
			if (e.getSource() == jbtMemory)	{
				c.memory = c.ans;
				screen.memory = true;
				screen.repaint();
			}
			// Memory-Recall Button
			if (e.getSource() == jbtMemoryRecall && screen.appendable == true && screen.memory == true && screen.memoryRecalled  == false)	{		// check if ScreenPanel is appendable & something has been loaded into memory, & the memory has not already been recalled
				screen.appendString(Double.toString(c.memory));
				screen.postDecimal = true;
				screen.memoryRecalled = true;
			}
			// Equals Button					// Note: checks to the value of currentOperator to decide what function to perform
			if (e.getSource() == jbtEquals)	{
				
				// These IF statements will:
				//		have the ScreenPanel return the first element and store it in the Calculator object
				// 		have the ScreenPanel return the second element and store it in the Calculator object
				// 		set the ScreenPanel output to the product of the calculation
				//		make the screen unappendable
				//		reset the postDecimal sentinel
				//		reset the memoryRecalled sentinel
				//		reset the operatorPresent sentinel
				
				// Perform Addition
				if (currentOperator == '+')	{
					c.element1 = screen.getElement1(currentOperator);
					c.element2 = screen.getElement2(currentOperator);		
					screen.setString(Double.toString(c.add()));
					screen.appendable = false;
					screen.postDecimal = false;
					screen.memoryRecalled = false;
					screen.operatorPresent = false;
				}
				// Perform Subtraction
				if (currentOperator == '\u2014')	{
					c.element1 = screen.getElement1(currentOperator);
					c.element2 = screen.getElement2(currentOperator);
					screen.setString(Double.toString(c.subtract()));
					screen.appendable = false;
					screen.postDecimal = false;
					screen.memoryRecalled = false;
					screen.operatorPresent = false;
				}
				// Perform Multiplication
				if (currentOperator == '*')	{
					c.element1 = screen.getElement1(currentOperator);
					c.element2 = screen.getElement2(currentOperator);
					screen.setString(Double.toString(c.multiply()));
					screen.appendable = false;
					screen.postDecimal = false;
					screen.memoryRecalled = false;
					screen.operatorPresent = false;
				}
				// Perform Division
				if (currentOperator == '/')	{
					c.element1 = screen.getElement1(currentOperator);		
					c.element2 = screen.getElement2(currentOperator);
					screen.setString(Double.toString(c.divide()));
					screen.appendable = false;
					screen.postDecimal = false;
					screen.memoryRecalled = false;
					screen.operatorPresent = false;
				}
			}
		}
		
	}
}


/** Screen Panel (GraphicsPanel) */		// Note: This GraphicsPanel is designed to function as an output Screen for the calculator
@SuppressWarnings("serial")
class ScreenPanel extends JPanel	{
	
	/** Data Fields */		// Note: most of these data fields are meant to be sentinel values which will switch certain behaviors on and off, as needed, by the calculator
	
	protected String s = new String("0");			// the output string.  This value will be displayed on the screen
	
	protected final byte MAX_LENGTH = 34;
	
	protected boolean operatorPresent = false;		// tracks if an operator is present on screen
	protected boolean postDecimal = false;			// tracks if a Decimal-Point has already been placed in the current number, to prevent more than one decimal per number
	protected boolean appendable = true;			// decides if new numbers can be added to the screen
	protected boolean memory = false;				// tracks if a number has been stored in the memory
	protected boolean memoryRecalled = false;		// tracks if the number stored in memory has already been displayed on the screen
	
	
	/** paintComponent Method */		// Note: draws graphics on screen
	@Override
	protected void paintComponent(Graphics g)	{
		super.paintComponent(g);
		
		g.setColor(new Color(223, 255, 187));								// sets the drawing color to an olive color for the string.
		g.fill3DRect(10, 10, getWidth() - 20, getHeight() - 20, false);		// draw screen
		g.setColor(Color.BLACK);
		
		// draw output string.  Set position of the string to the bottom right of the screen.  Note: - (s.length() * 8) adjusts the top left coordinate so 
		//		that no matter what the length of the string, the right side will remain stationary.
		g.drawString(s, (getWidth() - 25) - (s.length() * 8), getHeight() - 20);
		
		// check to see if something has been loaded into the memory
		if (memory == true)	{
			g.drawString("M", 20, 30);
		}
	}
	
	/** setString Method */				// replaces the values of the output string
	protected void setString(String str)	{
		// if the String to be output is too long, print 'Error' on the screen.
		if (str.length() > MAX_LENGTH)	{
			s = "Error";
		}
		// otherwise, replace the current output stirng with the new output
		else	{
			s = str;
			repaint();
		}
	}
	
	/** appendString Method */			// appends data to the output string
	protected void appendString(String str)	{
		// check to see if the maximum length has already been reached.  if so, refuse to append more.
		if (s.length() == MAX_LENGTH)	{
			return;
		}
		// check to see if the string is a '0', if so replace the entire string.
		if (s.compareTo("0") == 0)	{
			s = str;
		}
		// otherwise append the new string to the current string
		else	{
			StringBuilder build = new StringBuilder();		// create a StringBuilder to construct the new string
			build.append(s);								// add the current string to the StringBuilder
			build.append(str);								// add the new string to the StringBuilder
			s = build.toString();							// assign the StringBuilder to the output string
		}
		repaint();
	}
	
	/** backspaceString Method */		// erases one character from the string
	protected void backspaceString()	{
		StringBuilder erase = new StringBuilder();			// create a StringBuilder to construct the new string
		erase.append(s);									// add the current string to the StringBuilder
		erase.deleteCharAt(erase.length() - 1);				// remove the last character from the StringBuilder
		s = erase.toString();								// assign the StringBuilder to the output string
		repaint();
	}

	/** getElement1 Method */			// returns the first number in the equation
	protected double getElement1(char currentOperator)	{
		int index = s.indexOf(currentOperator);				// find the location of the currentOperator
		String sub = s.substring(0, index);					// store the substring from the beginning to the currentOperator
		double element = Double.parseDouble(sub);			// parse the substring
		return element;										// return the element which was stored in the substring
	}
	
	/** getElement2 Method */			// return the second number in the equation
	protected double getElement2(char currentOperator)	{
		int index = s.indexOf(currentOperator);				// find the location of the currentOperator
		String sub = s.substring(index + 1);				// store the substring from after the currentOperator to the end
		double element = Double.parseDouble(sub);			// parse the substring
		return element;										// return the element which was stored in the substring
	}
	
	/** negate Method */				// flips the positivity of the current number in equation
	protected void negate(char currentOperator)	{
		
		// if there is currently an operator in the string
		if (operatorPresent == true)	{
			int index = s.indexOf(currentOperator);			// find the location of the currentOperator
			String sub1 = s.substring(0, index + 1);		// store the substring from the beginning to the currentOperator (inclusive)
			String sub2 = s.substring(index + 1);			// store the substring from after the currentOperator to the end
			StringBuilder build = new StringBuilder();		// create a StringBuilder to build the new string
			build.append(sub1);								// add the first substring to the StringBuilder
			double oldNumber = Double.parseDouble(sub2);	// parse and store the second substring
			double newNumber = -oldNumber;					// flip the positivity of the second substring
			build.append(Double.toString(newNumber));		// add the second substring to the StringBuilder
			s = build.toString();							// assign the StringBuilder to the output string
			repaint();
		}
		// if there is NOT currently an operator in the string
		else	{
			double oldNumber = Double.parseDouble(s);		// parse and store the string
			double newNumber = -oldNumber;					// flip the positivity of the string
			s = Double.toString(newNumber);					// assign the new string to the output string
			repaint();
		}
	}
}

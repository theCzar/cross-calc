
all: jar 

cross-calc:
	mkdir -p bin
	javac -d bin -cp src src/cross_calc/CrossCalculator.java

jar: cross-calc
	jar cvfm CrossCalc.jar Manifest -C bin/ .

install: 
	cp CrossCalc.jar ~/Desktop

clean:
	rm -rf bin/*
	rm -f CrossCalc.jar

